package com.demo.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class AdditionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		int number1=Integer.parseInt(request.getParameter("no1"));
		int number2=Integer.parseInt(request.getParameter("no2"));
		
		int result=0;
		
		if(request.getParameter("r1").equals("Add"))
		{
		    result=number1+number2;
		}
		
		
		if(request.getParameter("r1").equals("Sub"))
		{
		    result=number1-number2;
		}
		
		if(request.getParameter("r1").equals("Mul"))
		{
		    result=number1*number2;
		}
		
		if(request.getParameter("r1").equals("Div"))
		{
		    result=number1/number2;
		}
		
		
			out.println("<h1>Result :"+result+"<h1>");
		
	}

	
}
